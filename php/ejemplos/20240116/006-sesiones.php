<?php
// inicializamos la sesion
session_start();

$_SESSION['visitas'] = 1;

// recuperar el nombre de la session
echo "nombre: " . session_name();
// recuperar el id de la sesion
echo "<br>id: " . session_id();

// recuperar el id de la session a traves de la cookie
echo "<br>id: " . $_COOKIE[session_name()];
