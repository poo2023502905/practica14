<?php
/**
 * Funcion que busca los elementos repetidos de un array
 * Devuelve un array con los elementos repetidos y cuantas veces se repiten
 *
 * @param array $array El array en el que se buscarán elementos repetidos.
 * @param bool $devolverTodos Si se deben devolver todos los elementos repetidos o no. El valor por defecto es falso.
 * @return array Un array con los elementos repetidos y sus conteos.
 */


// analizar la siguiente función y colocar un ejemplo de funcionamiento
// se deben colocar los comentarios necesario a la función
// (argumentos, descripción de la función, ejemplo de uso)


// corregir la siguiente función para que no de errores


// crear la función

function elementosRepetidos($array, $devolverTodos = false) {
    // inicializar el array
    $repeated =array();
// recorrer el array
    foreach ((array) $array as $value){
        $inArray=false;
// recorrer el array
        foreach($repeated as $i=>$rItem){
        if ( $rItem ['value']=== $value) {
            $inArray = true;
          ++$repeated[$i]['count'];
        }
           
} 
// añadir el elemento
if (false===$inArray){
    $i=count($repeated);
    $repeated[$i]= array();
    $repeated[$i]['value'] = $value;
    $repeated[$i]['count'] = 1;
}
    }
// devolver todos los elementos
if ($devolverTodos){
    foreach($repeated as $i=> $rItem){
        if($rItem['count']===1){
            unset($repeated[$i]);
        }
    }
  }
  // ordenar por conteo
sort($repeated);
return $repeated;
  }

var_dump( elementosRepetidos([1, 2, 2, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 5]));

?>