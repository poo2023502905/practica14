﻿CREATE DATABASE IF NOT EXISTS biblioteca;

USE biblioteca;

CREATE TABLE libros (
id int AUTO_INCREMENT PRIMARY KEY,
título varchar(200), 
paginas int (6),
fechaPublicacion date
);