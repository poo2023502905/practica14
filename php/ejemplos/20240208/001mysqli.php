<?php

// objetivo


// conectarme al servidor de base de datos mysql localhost
// seleccionar la base de datos personas
// realizar una consulta para mostrar la tabla persona
// UTILIZAMOS EL METODO FETCH_ALL
// mostrar los registros de la tabla persona
// cerrar conexión

// crear una conexion con el servidor utilizando la 
// clase mysqli

$conexion = new mysqli(
    'localhost', // host al que quiero conectarme
    'root', // usuario de la base de datos
    '', // contraseña
    'personas' // base de datos por defecto, esto es opcional
);

// la variable conexion es un OBJETO de tipo mysqli


// realizar una consulta para mostrar la tabla persona  
$resultados = $conexion->query("SELECT p.idPersona, p.nombre, p.edad FROM persona p");
// la variable resultado es un OBJETO de tipo mysqli_result


// mostrar los registros de la tabla persona
// $registros = $resultados->fetch_all();
// registros es una variable de tipo array BIDIMENSIONAL (enumerado en las dos dimensiones)

// quiero un array asociativo o mixto cambio el argumento
// MYSQLI_ASSOC, MYSQLI_NUM, o MYSQLI_BOTH.
// $registros = $resultados->fetch_all(MYSQLI_NUM); // esto es lo que hace por defecto
// $registros = $resultados->fetch_all(MYSQLI_BOTH);
$registros = $resultados->fetch_all(MYSQLI_ASSOC);
// registros es una variable de tipo array BIDIMENSIONAL (enumerado en la primera dimension y asociativo en la segundo)

// muestro el array
// con un foreach
foreach ($registros as $indice => $registro) {
?>
    <h2>Registro <?= $indice ?> </h2>
    <ul>
        <li>idPersona: <?= $registro['idPersona'] ?></li>
        <li>nombre: <?= $registro['nombre'] ?></li>
        <li>edad: <?= $registro['edad'] ?></li>
    </ul>

<?php
}

// mostrar el array con dos foreach
foreach ($registros as $indice => $registro) {
?>
    <h2>Registro: <?= $indice ?> </h2>
    <ul>
        <?php
        foreach ($registro as $nombreCampo => $valorCampo) {
        ?>
            <li><?= $nombreCampo ?>: <?= $valorCampo ?></li>
        <?php
        }
        ?>
    </ul>
<?php
}

// sin tener que abrir y cerrar php
foreach ($registros as $indice => $registro) {
    echo "<h2>Registro: {$indice}</h2>";
    echo "<ul>";
    foreach ($registro as $nombreCampo => $valorCampo) {
        echo "<li>{$nombreCampo}: {$valorCampo}</li>";
    }
    echo "</ul>";
}


// cerrar la conexion
// utilizando metodo close
$conexion->close();
