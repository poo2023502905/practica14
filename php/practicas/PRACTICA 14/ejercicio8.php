<?php

// crear una función que le pasas un array de numeros
// debe comprobar si en el array hay algu´n numero negativo
// en caso de que lo haya te devolverá "hay negativos"
// si no hay ninguno te devolverá "todos positivos"

function hay_negativos($numeros) {
    foreach ($numeros as $numero) {
        if ($numero < 0) {
            return "hay negativos";
        }
    }
    return "todos positivos";
}

// llamamos a la función

$numeros = array(1, 2, 3, 4, -5, 6, 7, 8, 9, 10);
echo hay_negativos($numeros);
