<?php
class Humano
{
    // atributos
    public string $nombre;
    public string $sexo;
    public string $fechaNacimiento;

    // constructor
    public function __construct(string $nombre = "", string $sexo = "", string $fechaNacimiento = "")
    {
        $this->nombre = $nombre;
        $this->sexo = $sexo;
        $this->fechaNacimiento = $fechaNacimiento;
    }

    // metodos
    // el metodo presentarse devolver nombre y fechaNacimiento
    public function presentarse(): string
    {
        return "{$this->nombre} {$this->fechaNacimiento}";
    }
}
