<?php

// precarga de clases
spl_autoload_register(function ($clase) {
    require 'clases/' . $clase . '.php';
});

// quiero introducir lo siguiente
// personas
// jorge,pedro,maria

// oficios
// programador,10 euros hora, 50 horas semanales
// ingeniero,20 euros hora, 50 horas semanales

// donde trabaja cada uno
// jorge programador
// pedro ingeniero  
// maria programador

$persona1 = new Humano('jorge');
$persona2 = new Humano('pedro');
$persona3 = new Humano('maria');

$oficio1 = new Oficio('programador', 10, 50);
$oficio2 = new Oficio('ingeniero', 20, 50);

$trabajan1 = new Trabajan($persona1, $oficio1);
$trabajan2 = new Trabajan($persona2, $oficio2);
$trabajan3 = new Trabajan($persona3, $oficio1);

echo "Persona1:" . $trabajan1->persona->presentarse();
echo "<br>Persona1: " . $trabajan1->oficio->calcular();
