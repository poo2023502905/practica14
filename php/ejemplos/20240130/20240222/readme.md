# APLICACION PARA CREAR TABLON DE ANUNCIOS

El usuario cuando entra a la aplicacion debe colocar su nombre.

Y una vez que ha colocado el nombre le debe permitir añadir mensajes.

A medida que añades mensajes debes ver los mensajes tuyos (con tu nombre) y los mensajes
que hayan colocado otros usuarios.

# PAGINA INDEX

Debe salir el nombre de usuario y el boton de entrar

![index](imgsReadme/index.png)

Una vez que se ha logueado debe poner el menu y Bienvenido y su nombre

# PAGINA MENSAJE

Quiero que me permita escribir mensajes.

En la parte inferior me debe listar todos los mensajes que hay en el sistema ademas colocar un boton que me permita eliminar cualquier mensaje.

![mensaje](imgsReadme/mensaje.png)

|Nombre usuario|Mensaje|Accion
|---|---|---|
|Ramon|Mensaje 1| ❌ Eliminar|
|Eva|Mensaje 1| ❌ Eliminar|

Cuando pulse eliminar un mensaje me deberia llevar a una pagina llamada eliminar que borrase ese mensaje y me redireccionaria a la pagina mensaje de nuevo.

# MENU

En todas las paginas debe colocarme un menu

- Inicio (pagina de index)
- Mensaje (pagina de mensaje)
- Salir (logout)

# CONSIDERERACIONES

Cuando el usuario no este logueado no puede ver el menu y no debe poder entrar a ninguna pagina excepto la del index para loguear

![menu](imgsReadme/menu.png)

# AYUDA

Seria recomendable tener una tabla para los mensajes (id,nombre,mensaje,fechaHora)

Necesitamos las variables de sesion para gestionar el login.
