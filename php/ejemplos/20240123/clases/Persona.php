<?php

class Persona
{
    // atributos
    public $nombre;
    public $apellidos;
    public $direccion;
    public $localidad;
    public $cp;

    // metodos
    public function hablar()
    {
        return "";
    }

    public function direccion()
    {
        return
            $this->direccion .
            " " .
            $this->localidad .
            " " .
            $this->cp;
    }

    // metodo que se ejecuta automaticamente cuando instancia la clase
    public function __construct()
    {
        // inicializa los atributos
        $this->nombre = "";
        $this->apellidos = "";
        $this->direccion = "";
        $this->localidad = "";
        $this->cp = "";
    }
}
