<?php

// crear una variable array
// donde voy a colocar cada vez que un jugador a
// metido un punto
// tengo 3 jugadores llamados
// p1, p2, p3
// quiero una tabla donde me indique el resultado
// de cada jugador
// realizarlo con for

$puntos = ['p1', 'p1', 'p2', 'p3', 'p2', 'p1', 'p1'];

// <table>
//     <tr>
//         <td>p1</td>
//         <td>4</td>
//     </tr>
//     <tr>
//         <td>p2</td>
//         <td>2</td>
//     </tr>
//     <tr>
//         <td>p3</td>
//         <td>1</td>
//     </tr>
// </table>

// utilizar variables

$p1 = 0;
$p2 = 0;
$p3 = 0;
// recorro el array y mediante un switch guardo el punto
for ($i = 0; $i < count($puntos); $i++) {
    switch ($puntos[$i]) {
        case 'p1':
            $p1++;
            break;
        case 'p2':
            $p2++;
            break;
        case 'p3':
            $p3++;
            break;
    }
    // otra forma de realizarlo
    // $valor = $puntos[$i];
    // $$valor++;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="estilos.css">
</head>

<body>
    <table>
        <tr>
            <th>p1</th>
            <td><?= $p1 ?></td>
        </tr>
        <tr>
            <th>p2</th>
            <td><?= $p2 ?></td>
        </tr>
        <tr>
            <th>p3</th>
            <td><?= $p3 ?></td>
        </tr>
    </table>
</body>

</html>