<?php
// crear un formulario que nos permita introducir un carácter
// y cuando pulsemos sobre el botón de enviar nos debe indicar si ese carácter es una vocal

?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Verificador de Vocales</title>
</head>
<body>
    <h1>Verificador de Vocales</h1>
    <form action="/procesar-caracter" method="POST">
        <label for="caracter">Introduce un carácter:</label>
        <input type="text" id="caracter" name="caracter" maxlength="1" required>
        <input type="submit" value="Verificar">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $caracter = $_POST["caracter"];
        if (preg_match("/^[aeiouAEIOU]$/", $caracter)) {
            echo "<p>El carácter '$caracter' es una vocal.</p>";
        } else {
            echo "<p>El carácter '$caracter' no es una vocal.</p>";
        }
    }
    ?>
</body>
</html>
