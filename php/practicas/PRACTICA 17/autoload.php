<?php
// para realizar la autocarga de clases
spl_autoload_register(function ($clase) {
    require "{$clase}.php";
});
