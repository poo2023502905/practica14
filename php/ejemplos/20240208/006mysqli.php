<?php

use clases\Persona;

// objetivo

// conectarme al servidor de base de datos mysql localhost
// seleccionar la base de datos personas
// realizar una consulta para mostrar la tabla persona
// utilizamos el metodo FETCH_OBJECT
// pero con mi propia clase PERSONA
// mostrar los registros de la tabla persona
// cerrar conexión

require_once "autoload.php";

// conectarse al servidor
$conexion = new mysqli("localhost", "root", "", "personas");

// realizo la consulta
// creo un objeto mysqli_result
$resultados = $conexion->query("SELECT * FROM persona");

// para mostrar los resultados 
// voy a utilizar fecht_object

// mediante un while

// quiero que con fetch_object me cree un objeto de tipo Persona

$contador = 1;
while ($registro = $resultados->fetch_object("clases\Persona")) {
    echo "<h2>Registro numero {$contador}</h2>";
    echo $registro;
    $contador ++;
    // echo "<ul>";
    // echo "<li>IdPersona : {$registro->idPersona}</li>";
    // echo "<li>Nombre : {$registro->nombre}</li>";
    // echo "<li>Edad : {$registro->edad}</li>";
    // echo "</ul>";
    // $contador++;
}

// var_dump($registro);
// var_dump(new Persona(["idPersona" => 1]));
