<?php
// Realizar un formulario que envie los datos a otra pagina
// los datos son nombre y apellidos
// la otra pagina debe almacenar los datos en una variable de sesion llamada
// datos y mostrarlos por pantalla con un enlace para volver al formulario
// en caso de que se cargue la otra pagina directamente si no hay datos metidos anteriormente
// debe volver a cargar el formulario

// ademas del formulario nos debe mostrar los ultimos datos enviados
// en caso de que no haya ningun dato metido anteriormente que me muestre los campos vacios
// aprovechando la variable de sesion

session_start();


// si no existe la variable de session datos 
// entonces creamela vacia
if (!isset($_SESSION['datos'])) {
    $_SESSION['datos'] = [
        'nombre' => '',
        'apellidos' => ''
    ];
}


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="formularios.css">
</head>

<body>
    <form action="008-sesiones.php" method="post">
        <div>
            <label for="nombre">Nombre</label>
            <input type="text" name="nombre" id="nombre" title="introduce nombre" placeholder="Introduce tu nombre" required>
        </div>
        <br>
        <div>
            <label for="apellidos">Apellidos</label>
            <input type="text" name="apellidos" id="apellidos" title="introduce apellidos" placeholder="Introduce tus apellidos" required>
        </div>
        <br>
        <div>
            <button>Enviar</button>
        </div>
    </form>

    <h2>Datos introducidos la ultima vez</h2>
    <div class="etiqueta">
        <span class="etiqueta">Nombre </span> : <?= $_SESSION['datos']['nombre'] ?>
    </div>
    <br>
    <div class="etiqueta">
        <span class="etiqueta">Apellidos </span> : <?= $_SESSION['datos']['apellidos'] ?>
    </div>

</body>

</html>