<?php
session_start();

// comprobar si he pulsado el boton de enviar
if ($_POST) {
    $_SESSION['datos'] = [
        'nombre' => $_POST['nombre'],
        'apellidos' => $_POST['apellidos']
    ];
}

// si no existe la variable de session redirecciono al formulario
// entrar en la pagina 8 sin pasar por la 7
// redirecciona a la 7
if (!isset($_SESSION['datos'])) {
    // para redireccionar en php utilizamos la funcion header
    // esta funcion es de cabacera y por tanto
    // tiene que realizarse antes de mostrar cualquier contenido en la web
    header('Location: 007-sesiones.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="formularios.css">

</head>

<body>
    <h1>Datos introducidos</h1>
    <div class="etiqueta">
        <span class="etiqueta">Nombre </span> : <?= $_SESSION['datos']['nombre'] ?>
    </div>
    <br>
    <div class="etiqueta">
        <span class="etiqueta">Apellidos </span> : <?= $_SESSION['datos']['apellidos'] ?>
    </div>
    <br>
    <div>
        <a class="boton" href="007-sesiones.php">Volver</a>
    </div>
</body>

</html>