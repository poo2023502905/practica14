<?php

// 1 paso
// mostrar tres frutas en pantalla de forma aleatoria

// 2 paso 
// Cuando pulse el botón “jugar” un nueva pagina que me muestre las ocho frutas
// cada una de las frutas es un boton submit con nombre fruta

// 3 paso 
// Cuando pulsemos una fruta nos manda a la misma pagina 
// y debemos indicar si la fruta existia en la primera pagina
// la fruta que aciertas le coloca una imagen de sonrisa
// si fallas una imagen de triste


// 4 paso
// cuando adivinos las 3 frutas se acaba el juego 

// 5 paso 
// colocar un boton que nos mande a volver a jugar 
// colocandose en la primera pagina

$numeros = [1, 2, 5];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="formularios.css">
    <link rel="stylesheet" href="tragaperras.css">

</head>

<body>
    <form method="POST" action="012-sesiones.php">
        <div class="caja-frutas">
            <span><img src="imgs/<?= $numeros[0] ?>.svg"></span>
            <span><img src="imgs/<?= $numeros[1] ?>.svg"></span>
            <span><img src="imgs/<?= $numeros[2] ?>.svg"></span>
        </div>
        <br>

        <div>
            <button name="jugar">jugar</button>
        </div>

    </form>
</body>

</html>