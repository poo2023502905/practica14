<?php
require_once "funciones.php";

// cargo los parametros de aplicacion
$parametros = require_once("parametros.php");

// desactivar errores
controlErrores();


// creo un array con los elementos
// que quiero que tenga el menu
$elementosMenu = [
    "Inicio" => "index.php",
    "Insertar" => "insertar.php"
];

// preparo el menu
echo menu($elementosMenu);

// conexion a base de datos
$conexion = @new mysqli(
    $parametros["bd"]["servidor"],
    $parametros["bd"]["usuario"],
    $parametros["bd"]["password"],
    $parametros["bd"]["nombreBd"]
);

// compruebo si la conexion es correcta
if ($conexion->connect_error) {
    die("Error de conexión: " . $conexion->connect_error);
}

// inicializo la salida de la vista
$salida = "";

// creo el array de datos vacio para mandar al formulario
// en caso de que no haya pulsado el boton de insertar
$datos = [
    "titulo" => "",
    "paginas" => 0,
    "fechaPublicacion" => ""
];

// tengo que comprobar si vengo o no de pulsar el boton insertar en el formulario

if ($_POST) {
    // si he pulsado el boton leo los datos que envio desde el formulario

    foreach ($datos as $clave => $valor) {
        $datos[$clave] = $_POST[$clave];
    }

    // foreach($_POST as $clave => $valor){
    //     $datos[$clave] = $valor;
    // }

    // tengo que insertar el registro en la bbdd
    $query = "INSERT INTO libros(titulo,paginas,fechaPublicacion) VALUES ('{$datos["titulo"]}',{$datos["paginas"]},'{$datos["fechaPublicacion"]}')";

    // ejecuto la consulta
    if ($conexion->query($query)) {
        $salida = "Registro insertado";
        $sql="SELECT * FROM libros where id={$conexion->insert_id}";
        $resultado = $conexion->query($sql);
        $salida.= gridView($resultado);
    } else {
        $salida = "Error insertando el registro" . $conexion->error;
    }
}



?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Biblioteca</title>
</head>

<body>
    <h1><?= $parametros["aplicacion"]["nombreAplicacion"] ?> - Insertar</h1>
    <?= $menu?>

    <?php
    if(!$_POST){
        require "_form.php";
    }
    else{
        echo $salida;
    }
    ?>

    <?= $salida ?>
</body>

</html>