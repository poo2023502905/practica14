<?php

// crear una función que le pasas un array de numeros y te los imprime

function imprimir_array($array) {
    foreach ($array as $num) {
        echo $num . " ";
    }       
}
    
// Llamamos a la función para imprimir los números
imprimir_array([1, 2, 3, 4, 5]);

