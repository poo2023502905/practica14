﻿DROP DATABASE IF EXISTS discos;
CREATE DATABASE discos;
USE discos;

CREATE TABLE disco (
id int AUTO_INCREMENT PRIMARY KEY,
titulo varchar (50),
autor varchar (25),
anyo int(4));

USE discos;
INSERT INTO disco ( titulo, autor, anyo)
  VALUES ('Super', 'mio',2024),('Varios','Genial',2023);