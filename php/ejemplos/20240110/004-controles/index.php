<?php
// cargo el controlador para que controle
require_once "controlador.php";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    // cargo el formulario
    require "_form.php";

    // si he mandado datos
    if ($_POST) {
        require "_resultados.php";
    }
    ?>

</body>

</html>