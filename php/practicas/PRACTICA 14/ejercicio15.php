<?php

// crear una función que le pasas n numeros como un array
// y te devuelve el producto de todos ellos

function multiplicaNumeros($array) {
    $multiplicacion = 1;
    for ($i = 0; $i < count($array); $i++) {
        $multiplicacion *= $array[$i];
    }
    return $multiplicacion;
}
// llamamos a la función para imprimir los números
echo multiplicaNumeros([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]) . "\n";
