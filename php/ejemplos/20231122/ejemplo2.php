<?php
/*
crear las variables e inicializarlas
*/
$titulo = "Mi primera página web";
$numero = 1;
$resultado =0;

/*
procesamiento
*/

$numero++; //esto es igual a 2
// esto es lo mismo que $numero=$numero+1;
$numero +1; //esto es igual a 2
?>
$numero=<?= $numero ?><br>
$resultado=<?=$resultado ?><br>
<?php

$numero + 1; // $numero es 2
$numero = $numero + 1; 
?>
$numero=<?= $numero ?><br>
$resultado=<?=$resultado ?><br>
<?php

++$numero; // es 4, porque va guardando el resultado anterior
?>
$numero=<?= $numero ?><br>
$resultado=<?=$resultado ?><br>
<?php

$resultado =$numero+1;
// $numero es 5
// $resultado es 5
?>
$numero=<?= $numero ?><br>
$resultado=<?=$resultado ?><br>
<?php
$resultado =$numero++;
//$numero es 6
// $resultado es 5
?>

$numero=<?= $numero ?><br>
$resultado=<?=$resultado ?><br>


<h1>
    <?=$titulo ?>
</h1> 
