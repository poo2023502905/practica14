<?php

// -- UTILIZAMOS FETCH_ASSOC --

// mostrar todos los datos de la tabla personas
// de la base de datos personas1
// del servidor de base de datos que esta en localhost

// Quiero que utilicemos el metodo fetch_assoc
// utilizando un array asociativo para cada registro

require_once "funciones.php";
require_once "controlErrores.php";

// Definir la información de conexión a la base de datos
$host = "localhost";
$usuario = "root";
$contrasena = "";
$base_de_datos = "personas1";

// 1 paso 
// realizar la conexion
$conexion = @new mysqli($host, $usuario, $contrasena, $base_de_datos);

// 2 paso 
// comprobar que la conexion se ha realizado correctamente
if ($conexion->connect_error) {
    die("La conexion fallo: " . $conexion->connect_error);
}

// 3 paso
// realizar la consulta
$consulta = "select * from personas";
$resultados = $conexion->query($consulta);

// 4 paso 
// colocar los resultados en pantalla
// utilizar el metodo fetch_array para leer los registros
// utilizamos un while
while ($registro = $resultados->fetch_assoc()) {
    // tener en cuenta que
    // $resultados->fetch_array(MYSQLI_ASSOC) ==== $resultados->fetch_assoc();

    // registro es un array unidimensional de tipo asociativo con los datos de un registro
    // echo detailView($registro);
    // puedo almacenar cada registro en un array y despues lo muestro con gridViewTable
    $registros[] = $registro;
}

// este gridviewtable funciona siempre
echo gridViewTable($registros);
// este gridviewtable solo funciona si le paso un array de arrays
echo gridViewTable1($registros);

// 5 paso
// cerrar la conexion
$conexion->close();
