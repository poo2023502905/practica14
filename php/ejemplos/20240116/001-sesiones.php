<?php

/*
TEORIA DE SESIONES
La función session_start() en PHP se utiliza para iniciar una nueva sesión o reanudar
la sesión existente en el servidor.
Las sesiones en PHP permiten almacenar datos en el servidor que están asociados con
un usuario específico a lo largo de múltiples solicitudes.
Esto es útil para mantener la información del usuario entre páginas web,
omo datos de inicio de sesión, carritos de compras, etc.
*/
// para poder utilizar sesiones necesito arrancar la sesion
session_start();

if (isset($_SESSION["visitas"])) {
    $_SESSION["visitas"]++;
} else {
    $_SESSION["visitas"] = 1;
}

echo $_SESSION["visitas"];

// if (isset($a)) {
//     $a++;
// } else {
//     $a = 1;
// }
// echo $a;
