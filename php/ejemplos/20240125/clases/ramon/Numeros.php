<?php

namespace clases\ramon;

class Numeros
{
    public array $numeros;
    public function __construct(array $numeros = [])
    {
        $this->numeros = $numeros;
    }

    // metodo para calcular la media de los numeros
    public function calcularMedia(): float
    {
        $media = 0;
        // opcion 1
        // utilizar la instruccion foreach
        // foreach ($this->numeros as $numero) {
        //     $media += $numero;
        // }

        // opcion 2
        //utilizar la funcion array_sum
        $media = array_sum($this->numeros);

        $media = $media / count($this->numeros);

        return $media;
    }

    public function calcularModa(): int
    {
        $frecuenciaMaxima = 0;
        $moda = 0;

        // opcion 1
        // con array_count_values y foreach
        // $frecuencias = array_count_values($this->numeros);

        // foreach ($frecuencias as $indice => $frecuencia) {
        //     if ($frecuencia > $frecuenciaMaxima) {
        //         $frecuenciaMaxima = $frecuencia;
        //         $moda = $indice;
        //     }
        // }

        // opcion 2
        // utilizando la funcion array_count_values, max, array_search
        $frecuencias = array_count_values($this->numeros);
        $frecuenciaMaxima = max($frecuencias);
        $moda = array_search($frecuenciaMaxima, $frecuencias);

        return $moda;
    }


    public function calcularMediana(): int
    {
        $mediana = 0;
        return $mediana;
    }
}
