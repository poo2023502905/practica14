<?php

$nota = 4;

// indicar si esta aprobado o suspenso
// si la nota es mayor o igual que 5 es aprobado en caso contrario suspenso

if ($nota >= 5) {
    echo 'aprobado';
} else {
    echo 'suspenso';
}

// opcion 1
echo ($nota >= 5) ? 'aprobado' : 'suspenso';

// opcion 2


if ($nota >= 5) {
    $resultado = 'aprobado';
} else {
    $resultado = "suspenso";
}

$resultado = ($nota >= 5) ? 'aprobado' : 'suspenso';
echo $resultado;
