<?php

use clases\Persona;

spl_autoload_register(function ($nombreClase) {
    include $nombreClase . '.php';
});

$objeto = new Persona("Juan", "Perez", "100000", 2000);
$objeto1 = new Persona("Pedro", "Lopez", "200000", 2001);

echo $objeto->imprimir();
echo $objeto1->imprimir();
