<?php

$a = [
    "nombre" => "ramon",
    "apellidos" => "abramo"
];

// devuelve la creacion del array como un string
$b = var_export($a, true);

// convierte un array a string con un caracter de separacion
$c = implode(",", $a);

// crear variables con los indices del array asociativo
extract($a, EXTR_PREFIX_ALL, "alpe");

// esto realiza lo mismo que extract
// foreach ($a as $key => $value) {
//      $key ="alpe_" . $key;
//      $$key = $value;
// 
// }



var_dump($b);
var_dump($c);
var_dump($alpe_nombre);
var_dump($alpe_apellidos);
