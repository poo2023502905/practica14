<?php

// creo un array con una serie de numeros y quiero
// impimirlo en una tabla donde cada 
// fila es un numero
// utilizamos for
$numeros = [1, 1, 2, 1, 3, 4, 5, 2];

// <table>
//     <tr>
//         <td>1</td>
//     </tr>
//     <tr>
//         <td>1</td>
//     </tr>
//     <tr>
//         <td>2</td>
//     </tr>
// </table>

$longitud = count($numeros);
echo "<table border='1'>";
for ($indice = 0; $indice < $longitud; $indice++) {
    echo "<tr><td>{$numeros[$indice]}</td></tr>";
}
echo "</table>";

?>
<hr>
<table border="1">
    <?php
    for ($indice = 0; $indice < $longitud; $indice++) {
    ?>
        <tr>
            <td> <?= $numeros[$indice] ?></td>
        </tr>
    <?php
    }
    ?>
</table>

<hr>
<?php
$salida = "";
$salida = "<table border='1'>";
for ($indice = 0; $indice < $longitud; $indice++) {
    $salida .= "<tr><td>{$numeros[$indice]}</td></tr>";
}
$salida .= "</table>";

echo $salida;
?>