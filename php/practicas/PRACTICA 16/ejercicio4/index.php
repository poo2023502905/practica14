<?php

use clases\Superheroe;

spl_autoload_register(function ($clase) {
    include $clase . '.php';
});

$heroe1 = new Superheroe("batman");
$heroe1->setDescripcion("el mejor");
$heroe1->setCapa(true);

// cuando imprimo el objeto entonces
// llama al metodo magico toString
// si el metodo no existe, produce una excepcion
echo $heroe1;
