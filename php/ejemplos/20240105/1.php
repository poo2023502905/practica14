<?php

$grados = [
    ['a', false],
    ['b', false],
    ['c', false],
    ['d', false],
    ['e', true],
];

$a = array_filter($grados, function ($grado) {
    return $grado[1];
});
$rango = array_pop($a);
$letra = $rango[0];
