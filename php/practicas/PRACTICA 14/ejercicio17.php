<?php

// crear una función que le pasas un string como argumento
// y te devuelve el numero de vocales que tiene el string

function contarVocales($string) {
    $vocales = "aeiouAEIOU";
    $cont = 0;
    for ($i = 0; $i < strlen($string); $i++) {
        if (strpos($vocales, $string[$i]) !== false) {
            $cont++;
        }
    }
    return $cont;
}

// Llamamos a la función para contar las vocales
echo contarVocales("hola mundo");



