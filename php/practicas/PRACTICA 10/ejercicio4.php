<?php
// crear un formulario que nos permita introducir un numero. Al pulsar sobre el botón nos debe indicar si ese
// numero es par o impar
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Verificador de Paridad</title>
</head>
<body>
    <h1>Verificador de Paridad</h1>
    <form  method="POST">
        <label for="numero">Introduce un número:</label>
        <input type="number" id="numero" name="numero" required>
        <input type="submit" value="Verificar">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $numero = $_POST["numero"];
        if ($numero % 2 == 0) {
            echo "<p>El número $numero es par.</p>";
        } else {
            echo "<p>El número $numero es impar.</p>";
        }
    }
    ?>
</body>
</html>

