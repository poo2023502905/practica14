<?php
// el post esta vacio
var_dump($_POST);

// el files contiene los metadatos de los ficheros subidos
var_dump($_FILES);

// inicializo las variables

// con array asociativo
$mensaje = [
    "pdf" => "",
    "imagen" => ""
];

// compruebo si he pulsado el boton de enviar
if ($_FILES) {
    // creo un array con todos los metadatos del fichero

    // si queremos un array asociativo 
    $archivos = [
        "pdf" => $_FILES["pdf"],
        "imagen" => $_FILES["imagen"]
    ];

    // ruta donde guardo los archivos subidos dentro del servidor

    $rutasDestino = [
        "pdf" => "./pdf/" . $archivos["pdf"]["name"],
        "imagen" => "./imgs/" . $archivos["imagen"]["name"] // $imagen["name"]
    ];

    // para mover el archivo de la carpeta temporal
    // donde lo coloca el servidor a la ruta destino

    $ficherosSubidos = [
        "pdf" => move_uploaded_file($archivos["pdf"]["tmp_name"], $rutasDestino["pdf"]),
        "imagen" => move_uploaded_file($archivos["imagen"]["tmp_name"], $rutasDestino["imagen"])
    ];


    if ($ficherosSubidos["pdf"]) {
        $mensaje["pdf"] = "Se ha subido el pdf correctamente";
    } else {
        $mensaje["pdf"] = "No se ha podido subir el pdf";
    }

    if ($ficherosSubidos["imagen"]) {
        $mensaje["imagen"] = "Se ha subido la imagen correctamente";
    } else {
        $mensaje["imagen"] = "No se ha podido subir la imagen";
    }
}



?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form method="post" enctype="multipart/form-data">
        <input type="file" name="pdf">
        <br>
        <input type="file" name="imagen">
        <br>
        <button>Enviar</button>
    </form>

    <div>
        <?= $mensaje["pdf"] ?>
    </div>
    <div>
        <?= $mensaje["imagen"] ?>
    </div>


</body>

</html>