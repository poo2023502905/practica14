<?php
// carga la clase empleado en el espacio de nombres actual
use src\Cliente;
use src\Empleado;
use src\Directivo;

// para poder utilizar sesiones
session_start();

// para carga automatica de clases
require_once "autoload.php";

// he pulsado el boton de almacenar empleado
if (isset($_POST["empleado"])) {
    // tengo que pasar los datos del array POST (asociativo) a un objeto de tipo empleado
    $empleado = new Empleado();
    $empleado->asignar($_POST);
}

if (isset($_POST["cliente"])) {
    $cliente = new Cliente();
    $cliente->asignar($_POST);
}
if (isset($_POST["directivo"])) {
    $directivo = new Directivo();
    $directivo->asignar($_POST);
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    // cargar el formulario de empleado
    require "_empleado.php";
    // cargar el formulario de cliente
    require "_cliente.php";

    require "_directivo.php";
    ?>
    <hr>
    <h2>Ultimo registro introducido</h2>
    <?php
    // si he rellenado el formulario de empleado anteriormente
    if (isset($empleado)) {
        echo "<h3>Empleado</h3>";
        echo $empleado; // llamo al metodo toString
        // guardo el objeto en la sesion serializado
        $_SESSION["empleado"][] = serialize($empleado);
    }

    // si he rellenado el formulario de cliente anteriormente
    if (isset($cliente)) {
        echo "<h3>Cliente</h3>";
        echo $cliente; // llamo al metodo toString
        // guardo el objeto en la sesion serializado
        $_SESSION["cliente"][] = serialize($cliente);
    }

    if (isset($directivo)) {
        echo "<h3>Directivo</h3>";  
        echo $directivo; // llamo al metodo toString
        // guardo el objeto en la sesion serializado
        $_SESSION["directivo"][] = serialize($directivo);

    }
    // quiero que me muestre todos los registros introducidos 
    // en la sesion actual
    ?>
    <hr>
    <h2>Empleados introducidos</h2>
    <?php
    if (isset($_SESSION["empleado"])) {
        foreach ($_SESSION["empleado"] as $empleado) {
            $empleado = unserialize($empleado); // deserializo el objeto
            echo $empleado; // llamo al metodo toString
        }
    } else {
        echo "No hay empleados";
    }

    ?>
    <hr>
    <h2>Clientes introducidos</h2>
    <?php
    if (isset($_SESSION["cliente"])) {
        foreach ($_SESSION["cliente"] as $cliente) {
            $cliente = unserialize($cliente); // deserializo el objeto
            echo $cliente; // llamo al metodo toString
        }
    } else {
        echo "No hay clientes";
    }
    ?>
    <h2>Directivos Introducidos</h2>
    <?php
    if (isset($_SESSION["directivo"])) {
        foreach ($_SESSION["directivo"] as $directivo) {
            $directivo = unserialize($directivo); // deserializo el objeto
            echo $directivo; // llamo al metodo toString
        }
    } else {
        echo "No hay directivos";
    }

    ?>
    <hr>
</body>

</html>