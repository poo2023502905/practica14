<?php
// inicializo la salida a vacio
$resultado = "";
// compruebo si se ha pulsado el boton
if ($_POST) {
    // realizo los calculos
    $resultado = $_POST["numero"];
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form method="post">
        <div>
            <label for="numero">Numero</label>
            <input type="number" name="numero" id="numero" required>
        </div>
        <div>
            <button>Enviar</button>
        </div>
    </form>
    <div>
        <?= $resultado ?>
    </div>
</body>

</html>