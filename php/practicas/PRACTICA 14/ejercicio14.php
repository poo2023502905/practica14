<?php

// crear una función que le pasas n numeros como un array
// y te devuelve la suma de todos ellos


function sumaNumeros($array) {
    $suma = 0;
    for ($i = 0; $i < count($array); $i++) {
        $suma += $array[$i];
    }
    return $suma;
}

// Llamamos a la función para imprimir los números
sumaNumeros([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);

