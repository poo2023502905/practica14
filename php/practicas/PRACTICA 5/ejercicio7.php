<?php
$dia=6;
// realizar un código que me permite indicar a que día de la semana pertenece
// 1 lunes
// 2 martes
// 3 miercoles
//  4 jueves
// 5 viernes
// 6 sabado
// 7 domingo
// implementarlo utilizando if, switch un array
$dias=[
    1=>"lunes",
    2=>"martes",
    3=>"miercoles",
    4=>"jueves",
    5=>"viernes",
    6=>"sabado",
    7=>"domingo",
];

if ($dias[$dia])
{
    echo $dias[$dia];
}
else
{
    echo "no se";
}
// hacer lo mismo utilizando un switch
switch ($dia)
{
    case 1:
    echo $dias[1];
    break;
    case 2:
    echo $dias[2];
    break;
    case 3:
    echo $dias[3];
    break;
    case 4:
    echo $dias[4];
    break;
    case 5:
    echo $dias[5];
    break;
    case 6:
    echo $dias[6];
    break;
    case 7:
    echo $dias[7];
    break;
    default:
    echo "no se";
    break;
}

?>