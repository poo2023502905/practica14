<?php
// estos datos los sacaria de una bbdd
$ciudadesBBDD = [
    "Sa" => "Santander",
    "B" => "Barcelona",
    "T" => "Torrelavega"
];

// controlamos si se ha pulsado el boton
if ($_POST) {

    // recogemos los datos del formulario
    $email = $_POST["email"] ?: "";
    $password = $_POST["password"] ?: "";
    $mes = $_POST["mes"] ?: "";
    $acceso = isset($_POST["acceso"]) ? implode(",", $_POST["acceso"]) : "No has seleccionado ninguna forma de acceso";
    $ciudad = $_POST["ciudad"] ?: "";
    // convierto el codigo de la ciudad en su nombre
    $ciudad = $ciudadesBBDD[$ciudad] ?? "No has seleccionado ninguna ciudad";
    $navegadores = isset($_POST["navegadores"]) ? implode(",", $_POST["navegadores"]) : "No hay navegadores seleccionados";
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="formulario.css">
</head>

<body>
    <form method="post">
        <div>
            <label for="email">Email</label>
            <input type="email" id="email" name="email" placeholder="Email" title="email" required>
        </div>
        <br>
        <div>
            <label for="password">Password</label>
            <input type="password" id="password" name="password" placeholder="Password" title="password" required>
        </div>
        <br>
        <div>
            <div>Mes de acceso</div>
            <div>
                <input type="radio" id="enero" name="mes" value="enero" checked>
                <label for="enero">Enero</label><br>
                <input type="radio" id="febrero" name="mes" value="febrero">
                <label for="febrero">Febrero</label><br>
                <input type="radio" id="marzo" name="mes" value="marzo">
                <label for="marzo">Marzo</label><br>
            </div>
        </div>
        <br>
        <div>
            <div>Formas de acceso</div>
            <div>
                <input type="checkbox" id="telefono" name="acceso[]" value="telefono">
                <label for="telefono">Telefono</label><br>
                <input type="checkbox" id="ordenador" name="acceso[]" value="ordenador">
                <label for="ordenador">Ordenador</label><br>
            </div>
        </div>
        <br>
        <div>
            <div>
                <label for="ciudad">Ciudad</label>
            </div>
            <div>
                <select name="ciudad" id="ciudad" required>
                    <option value="" disabled selected>Selecciona una ciudad</option>
                    <option value="Sa">Santander</option>
                    <option value="B">Barcelona</option>
                    <option value="T">Torrelavega</option>
                </select>
            </div>
        </div>
        <br>
        <div>
            <div>
                <label for="navegadores">Navegadores</label>
            </div>
            <div>
                <select name="navegadores[]" id="navegadores" multiple size="3">
                    <option value="google">Google Chrome</option>
                    <option value="edge">Microsoft Edge</option>
                    <option value="Safari">Safari</option>
                </select>
            </div>
        </div>
        <div>
            <button>Enviar</button>
        </div>
    </form>

    <br>
    <?php
    // si has pulsado el boton de enviar muestro resultados
    if ($_POST) {
    ?>
        <div>
            <h2>Email</h2>
            <div><?= $email ?></div>
        </div>
        <div>
            <h2>Password</h2>
            <div><?= $password ?></div>
        </div>
        <div>
            <h2>Mes de acceso</h2>
            <div><?= $mes ?></div>
        </div>
        <div>
            <h2>Formas de acceso</h2>
            <div><?= $acceso ?></div>
        </div>
        <div>
            <h2>Ciudad</h2>
            <div><?= $ciudad ?></div>
        </div>
        <div>
            <h2>Navegadores</h2>
            <div><?= $navegadores ?></div>
        </div>

    <?php
    }
    ?>
</body>

</html>