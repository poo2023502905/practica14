<?php

use clases\Persona;
use clases\Personaconstructor;

// -- UTILIZAMOS FETCH_ASSOC pero con un objeto de tipo persona --

// mostrar todos los datos de la tabla personas
// de la base de datos personas1
// del servidor de base de datos que esta en localhost

// Quiero que utilicemos el metodo fetch_assoc
// utilizando un objeto de tipo Persona para cada registro
require_once "funciones.php";
require_once "autoload.php";
require_once "controlErrores.php";

// Definir la información de conexión a la base de datos
$host = "localhost";
$usuario = "root";
$contrasena = "";
$base_de_datos = "personas1";

// 1 paso 
// realizar la conexion

$conexion = @new mysqli($host, $usuario, $contrasena, $base_de_datos);

// 2 paso 
// comprobar que la conexion se ha realizado correctamente
if ($conexion->connect_error) {
    die("La conexion ha fallado: " . $conexion->connect_error);
}

// 3 paso
// realizar la consulta
$consulta = "select * from personas";
$resultados = $conexion->query($consulta);

// 4 paso 
// colocar los resultados en pantalla
// utilizamos un while

// $registro=$resultados->fetch_object();
// en registro tenemos un objeto de tipo stdClass
// donde tenemos los datos del registro

while ($registro = $resultados->fetch_assoc()) {
    $registroObjeto = new Personaconstructor($registro);
        


    $registros[] = $registroObjeto; // tenemos un array de objetos
}

echo gridViewTable($registros);


// 5 paso
// cerrar la conexion

$conexion->close();
