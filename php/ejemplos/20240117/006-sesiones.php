<?php
function mostrar($mensaje = "---", ...$numeros)
{
    $resultado = "<h2>$mensaje</h2>";
    $resultado .= "<table>";
    $resultado .= "<tr>";
    foreach ($numeros as $num) {
        $resultado .= ($num == "-") ? "<td>-</td>" : "<td class='acierto'>$num</td>";
    }
    $resultado .= "</tr>";
    $resultado .= "</table>";
    return $resultado;
}


function crear($minimo, $maximo)
{
    $numeros = range($minimo, $maximo);
    $longitud = rand(10, 20);
    shuffle($numeros);
    $numeros = array_slice($numeros, 0, $longitud);
    return $numeros;
}

// inicializar la sesion
session_start();

// se trata de realizar un pequeño juego donde
// el usuario tiene que adivinar numeros del array numeros
// que debo crear con numeros aleatorios entre 1 y 50 (pueden ser de 10 a 20 numeros)
// creamos un formulario donde escribo un numero y cuando pulso
// enviar debe comprobar si el numero esta en el array
// si esta en el array me indica que he acertado y me lo muestra
// si fallo me cuenta un fallo y me debe mostrar el numero de fallos
// cuando adivine los 10 numeros la puntuacion sera el numero de intentos 
// para adivinar los 10 numeros

// una vez que adivina los 10 numeros debe borrar las variables de sesion
// colocar un boton para reiniciar el juego cuando quieras



// leo las variables de session y asigno variables
$numeros = $_SESSION["numeros"] ?? crear(1, 50);
$aciertos = $_SESSION["aciertos"] ?? array_fill(0, count($numeros), '-');
$errores = $_SESSION["errores"] ?? 0;
$intentos = $_SESSION["intentos"] ?? 0;

// variable para controlar si seguimos jugando
// cuando final valga 1 necesito reiniciar el juego
$final = 0;


// comprobar que he pulsado el boton de enviar
if (isset($_POST["jugar"])) {

    // leo el numero si no esta vacio
    if (isset($_POST["numero"])) {
        $numero = $_POST["numero"];
        // comprobar si el numero esta en el array y no esta en aciertos
        if (in_array($numero, $numeros)) {
            // si esta en el array me coloca el acierto en aciertos en la misma posicion que a encontrado
            $aciertos[array_search($numero, $numeros)] = $numero;
            //elimino el numero acertado de numeros
            unset($numeros[array_search($numero, $numeros)]);
        } else {
            // añado un error
            $errores++;
        }
        // añado un intento
        $intentos++;
        // comprobar si se ha acabado el juego
        // si no hay mas numeros para adivinar
        if (count($numeros) == 0) {
            $final = 1;
        }
    }
    // vuelvo a colocar los valores en las variables de sesion
    $_SESSION["aciertos"] = $aciertos;
    $_SESSION["errores"] = $errores;
    $_SESSION["intentos"] = $intentos;
    $_SESSION["numeros"] = $numeros;
} elseif (isset($_POST["reiniciar"])) {
    $final = 1;
}


// compruebo si existe alguna condicion por la que se termina el juego
if ($final) {
    // borrar sesion
    session_unset();
    // borrar las variables de sesion
    unset($_SESSION["numeros"]);
    unset($_SESSION["aciertos"]);
    unset($_SESSION["errores"]);
    unset($_SESSION["intentos"]);
    // inicializamos las variables
    $aciertos = array_fill(0, count($numeros), '-');
    $errores = 0;
    $intentos = 0;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        table {
            border: 1px solid black;
            border-collapse: collapse;
            max-width: 100%;
        }

        td {
            border: 1px solid black;
            width: 100px;
            text-align: center;
            vertical-align: middle;
            padding: 10px;
        }

        label {
            background-color: gray;
            color: white;
            padding: 10px;
        }

        form,
        .salida {
            margin: 20px;
        }

        input,
        button {
            padding: 10px;

        }

        h2 {
            text-transform: uppercase;
        }

        .acierto {
            background-color: gray;
            color: white;
        }
    </style>
</head>

<body>
    <form method="post">
        <div>
            <label for="numero">Numero</label>
            <input type="number" id="numero" name="numero">
        </div>
        <br>
        <div>
            <button type="submit" name="jugar">Enviar</button>
            <button type="submit" name="reiniciar">Reiniciar</button>
        </div>
    </form>
    <div class="salida">
        <?php
        // mostrar numeros acertados
        // var_dump($aciertos);
        echo mostrar("aciertos", ...$aciertos);
        // mostrar errores
        // var_dump($errores);
        echo mostrar("errores", $errores);
        // mostrar intentos
        // var_dump($intentos);
        echo mostrar("intentos", $intentos);

        var_dump($_SESSION);
        ?>
    </div>
</body>

</html>