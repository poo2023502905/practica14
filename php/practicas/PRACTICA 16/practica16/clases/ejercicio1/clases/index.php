<?php
use clases\Persona;

spl_autoload_register(function($nombreClase)
{
    include $nombreClase.".php";
});
$objeto=new Persona("Juan","Perez","12345678",1990);
$objeto1=new Persona("Maria","Lopez","87654321",1995);
echo $objeto->imprimir();
echo $objeto1->imprimir();