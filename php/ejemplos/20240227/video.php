<?php

// cargando la clase en el espacio de nombres actual
use clases\Aplicacion;
use clases\Header;
use clases\Modelo;
use clases\Pagina;


// definiendo el autoload
spl_autoload_register(function ($clase) {
    include $clase . '.php';
});


// instanciando la clase
$aplicacion = new Aplicacion();
$favoritos = new Modelo($aplicacion->db);
$favoritos->query("select * from favoritos where categorias='video'");
$cabecera = Header::ejecutar([
    "titulo" => "Video",
    "subtitulo" => $aplicacion->configuraciones['autor'],
    "salida" => "Paginas sobre videos"
]);

Pagina::comenzar();
?>
<?= $favoritos->gridViewBotones(); ?>
<?php
Pagina::terminar([
    "titulo" => "video",
    "cabecera" => $cabecera,
    "pie" => "Creado por: " . $aplicacion->configuraciones['autor']
]);
