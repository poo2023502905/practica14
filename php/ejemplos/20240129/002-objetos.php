<?php

// opcion 1
use clases\animales\Animal;
use clases\animales\Perro;
use clases\animales\Persona;
use clases\animales\Vaca;

// opcion 2
// use clases\animales\{Animal,Perro,Persona,Vaca};

require_once 'autoload.php';

$animal1 = new Animal();
$persona1 = new Persona('juan', 'perez', 25);
$perro1 = new Perro('perrito', 12, 'blanco', true, $persona1);
$vaca1 = new Vaca('gustava');

var_dump($animal1);
var_dump($persona1);
var_dump($perro1);
var_dump($vaca1);
