<?php
// variables a utilizar
$a = 10;
$b = 3;
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Operadores</title>
</head>

<body>
    <?php
    // Quiero que coloquemos los resultados de las siguientes operaciones
    // a+b
    // a-b
    // a*b
    // a resto b
    // a elevado b
    // a dividido entre b
    // a es igual a b
    // a menor o igual que b

    // colocamos una tabla en donde la primera columa es el nombre
    // de la operacion
    // en la segunda columna el resultado

    //
    ?>
    <table border="1">
        <tr>
            <td>Operaciones</td>
            <td>Resultados</td>
        </tr>
        <tr>
            <td>Suma</td>
            <td><?php echo $a + $b; ?></td>
        </tr>
        <tr>
            <td>Restar</td>
            <td><?= $a - $b ?></td>
        </tr>
        <tr>
            <td>Producto</td>
            <td><?= $a * $b ?></td>
        </tr>
        <tr>
            <td>Resto</td>
            <td><?= $a % $b ?></td>
        </tr>
        <tr>
            <td>Elevado</td>
            <td><?= $a ** $b ?></td>
        </tr>
        <tr>
            <td>Cociente</td>
            <td><?= $a / $b ?></td>
        </tr>
        <tr>
            <td>A igual a B</td>
            <td>
                <?php
                if ($a == $b) {
                    echo "Si, son iguales";
                } else {
                    echo "No son iguales";
                }
                ?>
            </td>
        </tr>
        <tr>
            <td>A menor o igual que B</td>
            <td>
                <?php
                if ($a <= $b) {
                    echo "a es menor o igual que b";
                } else {
                    echo "a no es menor o igual que b";
                }
                ?>
            </td>
        </tr>
    </table>
</body>

</html>